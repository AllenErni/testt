/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "Planet.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Create your scene here :)
	//mCamera->setPolygonMode(PolygonMode::PM_WIREFRAME);

	// Center cube
	ManualObject* manual = createCube(10.0f);
	mObject = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mObject->attachObject(manual);

	// Make an anchor. Set the parent of the anchor to the center cube
	mRevolvingCubeAnchor = mObject->createChildSceneNode();

	// Revolving cube
	ManualObject* cube = createCube(5.0f);
	ManualObject* cube2 = createCube(5.0f);
	ManualObject* cube3 = createCube(5.0f);


	// ==============================================================================================

	testCube = Planet::createPlanet(*mSceneMgr, 5.0f, ColourValue::ColourValue(1, 1, 0));

	// ==============================================================================================

	//mRevolvingCube = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	// Instatiate the revolving cube as a child of the center cube
	mRevolvingCube = mRevolvingCubeAnchor->createChildSceneNode();
	mRevolvingCube->attachObject(cube);
	mRevolvingCube->translate(50, 0, 0);

	mRevolvingCube2 = mRevolvingCubeAnchor->createChildSceneNode();
	mRevolvingCube2->attachObject(cube2);
	mRevolvingCube2->translate(100, 0, 0);

	mRevolvingCube3 = mRevolvingCubeAnchor->createChildSceneNode();
	mRevolvingCube3->attachObject(cube3);
	mRevolvingCube3->translate(150, 0, 0);

	


	//mRevolvingCube4 = mRevolvingCubeAnchor->createChildSceneNode();
	//mRevolvingCube4->attachObject(cube2);
	//mRevolvingCube4->translate(200, 0, 0);

}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	// Rotation
	//Degree xRot;
	//if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8)) {
	//	xRot += Degree(45.0f * evt.timeSinceLastFrame);
	//}
	//else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2)) {
	//	xRot -= Degree(45.0f * evt.timeSinceLastFrame);
	//}

	Degree yRot;
//	if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4)) {
		yRot -= Degree(45.0f * evt.timeSinceLastFrame);
	//}
	//else if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6)) {
	//	yRot += Degree(45.0f * evt.timeSinceLastFrame);
	//}

	// Rotate the object using x and y rotations
	//mObject->rotate(Vector3(1.0, 0.0, 0.0), Radian(xRot));
	mObject->rotate(Vector3(0.0, 1.0, 0.0), Radian(yRot));
	mRevolvingCube->rotate(Vector3(0.0, 5.0, 0.0), Radian(yRot));
	mRevolvingCube2->rotate(Vector3(0.0, 5.0, 0.0), Radian(yRot));
	mRevolvingCube3->rotate(Vector3(0.0, 5.0, 0.0), Radian(yRot));

	//// Revolve the cube around a center point  cube 1
	Degree revolutionDegrees = Degree(45.0f * evt.timeSinceLastFrame); //change speed rev
	float oldX = mRevolvingCube->getPosition().x;
	float oldZ = mRevolvingCube->getPosition().z;

	float newX = (oldX * Math::Cos(Radian(revolutionDegrees))) + (oldZ * Math::Sin(Radian(revolutionDegrees)));
	float newZ = (oldX * -Math::Sin(Radian(revolutionDegrees))) + (oldZ * Math::Cos(Radian(revolutionDegrees)));

	mRevolvingCube->setPosition(newX, 0, newZ);

	mRevolvingCubeAnchor->yaw(Radian(revolutionDegrees));

	// cube2
	revolutionDegrees = Degree(80.0f * evt.timeSinceLastFrame);

	oldX = mRevolvingCube2->getPosition().x;
	oldZ = mRevolvingCube2->getPosition().z;

	newX = (oldX * Math::Cos(Radian(revolutionDegrees))) + (oldZ * Math::Sin(Radian(revolutionDegrees)));
	newZ = (oldX * -Math::Sin(Radian(revolutionDegrees))) + (oldZ * Math::Cos(Radian(revolutionDegrees)));

	mRevolvingCube2->setPosition(newX, 0, newZ);

	// cube 3

	revolutionDegrees = Degree(20.0f * evt.timeSinceLastFrame);

	oldX = mRevolvingCube3->getPosition().x;
	oldZ = mRevolvingCube3->getPosition().z;

	newX = (oldX * Math::Cos(Radian(revolutionDegrees))) + (oldZ * Math::Sin(Radian(revolutionDegrees)));
	newZ = (oldX * -Math::Sin(Radian(revolutionDegrees))) + (oldZ * Math::Cos(Radian(revolutionDegrees)));

	mRevolvingCube3->setPosition(newX, 0, newZ);



	return true;
}

ManualObject* TutorialApplication::createCube(float size)
{
	ManualObject* manual = mSceneMgr->createManualObject();
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	float zSize = size * 2;
	// define vertex position of index 0..3
	// Note that each time you call position() you start a new vertex. 
	// So you need to call colour everytime you define a vertex
	// See: http://www.ogre3d.org/docs/api/1.9/class_ogre_1_1_manual_object.html

	// Lecture: CORRECT THE VERTICES
	manual->position(-(size / 2), -(size / 2), size / 2);
	manual->colour(ColourValue::Blue);
	manual->position((size / 2), -(size / 2), size / 2);
	manual->colour(ColourValue::Blue);
	manual->position((size / 2), (size / 2), size / 2);
	manual->colour(ColourValue::Blue);
	manual->position(-(size / 2), (size / 2), size / 2);
	manual->colour(ColourValue::Blue);
	manual->position(-(size / 2), -(size / 2), -size / 2);
	manual->colour(ColourValue::Green);
	manual->position((size / 2), -(size / 2), -size / 2);
	manual->colour(ColourValue::Green);
	manual->position((size / 2), (size / 2), -size / 2);
	manual->colour(ColourValue::Green);
	manual->position(-(size / 2), (size / 2), -size / 2);
	manual->colour(ColourValue::Green);

	// define usage of vertices by refering to the indexes
	// Front
	manual->index(0);
	manual->index(1);
	manual->index(2);
	manual->index(3);
	manual->index(0);
	manual->index(2);
	// Back
	manual->index(6);
	manual->index(5);
	manual->index(4);
	manual->index(6);
	manual->index(4);
	manual->index(7);
	// Left
	manual->index(3);
	manual->index(4);
	manual->index(0);
	manual->index(3);
	manual->index(7);
	manual->index(4);
	// Right
	manual->index(5);
	manual->index(2);
	manual->index(1);
	manual->index(5);
	manual->index(6);
	manual->index(2);
	// Top
	manual->index(6);
	manual->index(3);
	manual->index(2);
	manual->index(3);
	manual->index(6);
	manual->index(7);
	// Bottom
	manual->index(1);
	manual->index(0);
	manual->index(5);
	manual->index(4);
	manual->index(5);
	manual->index(0);

	manual->end();
	return manual;
}

ManualObject* TutorialApplication::createTriangle()
{
	ManualObject* manual = mSceneMgr->createManualObject("manual");
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	manual->position(0.0f, 10.0f, -5.0f);
	manual->colour(ColourValue::Red);
	manual->position(-10.0f, -5.0f, -5.0f);
	manual->colour(ColourValue::Green);
	manual->position(10.0f, -5.0f, -5.0f);
	manual->colour(ColourValue::Blue);

	manual->index(0);
	manual->index(1);
	manual->index(2);

	manual->end();
	return manual;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception & e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
