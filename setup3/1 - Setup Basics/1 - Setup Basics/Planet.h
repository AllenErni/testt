#pragma once
#include <OgreManualObject.h>
//#include <OgreSceneMode.h>
#include <OgreSceneManager.h>
using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode* node);
	static Planet* createPlanet(SceneManager& sceneManager, float size, ColourValue colour);
	~Planet();


	SceneNode& getNode();



private:
	SceneNode* mNode;
	
};

