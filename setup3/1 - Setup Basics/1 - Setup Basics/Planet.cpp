#include "Planet.h"

Planet::Planet(SceneNode* node)
{
	mNode = node;
}

Planet* Planet::createPlanet(SceneManager& sceneManager, float size, ColourValue colour)
{
	ManualObject* obj = sceneManager.createManualObject();

	obj->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	float zSize = size * 2;
	// define vertex position of index 0..3
	// Note that each time you call position() you start a new vertex. 
	// So you need to call colour everytime you define a vertex
	// See: http://www.ogre3d.org/docs/api/1.9/class_ogre_1_1_manual_object.html

	// Lecture: CORRECT THE VERTICES
	obj->position(-(size / 2), -(size / 2), size / 2);
	obj->colour(ColourValue::Blue);
	obj->position((size / 2), -(size / 2), size / 2);
	obj->colour(ColourValue::Blue);
	obj->position((size / 2), (size / 2), size / 2);
	obj->colour(ColourValue::Blue);
	obj->position(-(size / 2), (size / 2), size / 2);
	obj->colour(ColourValue::Blue);
	obj->position(-(size / 2), -(size / 2), -size / 2);
	obj->colour(ColourValue::Green);
	obj->position((size / 2), -(size / 2), -size / 2);
	obj->colour(ColourValue::Green);
	obj->position((size / 2), (size / 2), -size / 2);
	obj->colour(ColourValue::Green);
	obj->position(-(size / 2), (size / 2), -size / 2);
	obj->colour(ColourValue::Green);

	// define usage of vertices by refering to the indexes
	// Front
	obj->index(0);
	obj->index(1);
	obj->index(2);
	obj->index(3);
	obj->index(0);
	obj->index(2);
	// Back
	obj->index(6);
	obj->index(5);
	obj->index(4);
	obj->index(6);
	obj->index(4);
	obj->index(7);
	// Left
	obj->index(3);
	obj->index(4);
	obj->index(0);
	obj->index(3);
	obj->index(7);
	obj->index(4);
	// Right
	obj->index(5);
	obj->index(2);
	obj->index(1);
	obj->index(5);
	obj->index(6);
	obj->index(2);
	// Top
	obj->index(6);
	obj->index(3);
	obj->index(2);
	obj->index(3);
	obj->index(6);
	obj->index(7);
	// Bottom
	obj->index(1);
	obj->index(0);
	obj->index(5);
	obj->index(4);
	obj->index(5);
	obj->index(0);

	obj->end();
	//SceneNode* node = sceneManager.getRootSceneNode()->createChildSceneNode();
	//node->attachObject(obj);
	//return new Planet(node);

	return obj;
}

SceneNode& Planet::getNode()
{
	return *mNode;
}
